module.exports = {
  // Override service name from package.json
  // Allowed characters: a-z, A-Z, 0-9, -, _, and space
  serviceName: "muchas" || process.env.PROJECT_NAME,
  // Use if APM Server requires a token
  secretToken: "" || process.env.APM_TOKEN,
  // Set custom APM Server URL (default: http://localhost:8200)
  serverUrl: "http://elastic-apm:8200" || process.env.APM_HOST,
  // Only activate the agent if it's running in production
  active: true,
};
