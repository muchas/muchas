import Server from "../../src/web/Server";

describe("Server basic", function() {
  it("Check christmas", async function() {
    const server = new Server({
      port: 8000,
    });
    const serverInit = await server.start();
    expect(serverInit).toHaveProperty("server");
    server.stop();
  });
});
