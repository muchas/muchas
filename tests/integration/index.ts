import muchas, { Component } from "../../src";

const component = new Component({
  baseUrl: "/",
  routes: [
    {
      method: "get",
      path: "/test",
      controller: (req, res): void => {
        res.json({ oi: 1 });
      },
    },
  ],
  messages: [
    {
      exchange: "muchasv3",
      queue: "muchasv3",
      routeKey: "muchasv3",
      action: (payload, done): void => {
        console.log(payload);
        done();
      },
    },
  ],
  routines: [
    {
      cron: "* * * * *",
      id: "routine_test",
      action: (job, done): void => {
        muchas.log.error(new Error("oi"));
        done();
      },
    },
  ],
});

// TODO: Remove, dev only
muchas.init({
  components: [component],
});
