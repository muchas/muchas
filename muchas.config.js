module.exports = {
  server: {
    enabled: true, // Default is true
    port: 8080,
    debugPort: 8181,
  },
  logger: {
    elastic: {
      // If you wish elastic logger to be enabled
      host: "http://elastic:9200",
      level: "info",
    },
  },
  database: {
    uri: "mongodb://mongo/muchas",
    options: {
      // Mongoose options  (https://mongoosejs.com/docs/connections.html#options)
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
  },
  broker: {
    host: "rabbit",
  },
  web: {
    port: 8080,
  },
  routines: true, // Enables the routines feature
};
