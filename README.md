# Muchas
[![pipeline status](https://gitlab.com/muchas/muchas/badges/master/pipeline.svg)](https://gitlab.com/muchas/muchas/commits/master) [![coverage report](https://gitlab.com/muchas/muchas/badges/master/coverage.svg)](https://gitlab.com/muchas/muchas/commits/master) ![npm](https://img.shields.io/npm/v/muchas)

Muchas is an opinionated framework to build services for build for a restricted stack

## Supported Stack
* Elasticsearch
* Elastic APM
* MongoDB
* Redis
* RabbitMQ
* Docker/Kubernetes

## Frameworks built-in
* ExpressJS
* Mongoose
* Amqplib
* Winston