import agenda from "agenda";
import os from "os";
import Routine from "./routine.interface";
import { Express } from "express";

/* eslint-disable-next-line */
const Agendash = require("agendash");

interface RoutinesOptions {
  mongoConString: string;
  app: Express;
}

export default class RoutineLoader {
  private Agenda: agenda;
  private app: Express;
  private apm: any;

  /**
   * Creates an instance of Routines.
   * @param {RoutinesOptions} options
   * @memberof Routines
   */
  constructor(options: RoutinesOptions, Apm?: any) {
    let opt = {};
    this.app = options.app;
    this.apm = Apm;
    if (options.mongoConString) {
      opt = {
        db: {
          address: options.mongoConString || null,
          collection: "routines",
          options: {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            autoReconnect: undefined,
            reconnectTries: undefined,
            reconnectInterval: undefined,
          },
        },
      };
    }
    this.Agenda = new agenda(opt);
    this.Agenda.name(`${os.hostname}-${process.pid}`);
    this.Agenda.start();

    this.app.use("/routines", Agendash(this.Agenda));
  }

  public async start() {
    return new Promise((resolve, reject) => {
      this.Agenda.on("ready", () => {
        this.Agenda.start();
        resolve();
      });
    });
  }

  /**
   * Add job
   *
   * @param {Routine} routine
   * @returns {Promise<void>}
   * @memberof Routines
   */
  public async addJob(routine: Routine): Promise<void> {
    const { id, cron, action, concurrency, lockLifetime, timezone } = routine;

    try {
      const opt: any = {};

      opt.concurrency = concurrency || 1;
      if (lockLifetime) {
        opt.lockLifetime = lockLifetime;
      }
      if (timezone) {
        opt.timezone = timezone;
      }

      this.Agenda.define(
        id,
        opt,
        async (job, done): Promise<void> => {
          let trans: any;
          if (this.apm) {
            trans = this.apm.startTransaction(id, "routine");
          }
          await action(job, (): void => {
            done();
            if (this.apm && trans) {
              trans.end();
            }
          });
        },
      );

      this.Agenda.every(cron, id, opt, opt);
    } catch (e) {
      throw new Error(e);
    }
  }

  /**
   * Stop the agenda routine
   *
   * @returns {Promise<void>}
   * @memberof Routines
   */
  public async stop(): Promise<void> {
    await this.Agenda.stop();
  }
}
