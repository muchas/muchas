import path from "path";

import IConfig from "./config.interface";

const defaultConfig: any = {
  logger: {
    elastic: undefined,
  },
};

const configLoader = async (): Promise<IConfig> => {
  try {
    const configMuchas = await require(path.join(process.cwd(), "muchas.config.js"));
    const packageJson = await require(path.join(process.cwd(), "package.json"));

    return {
      ...defaultConfig,
      ...(packageJson ? { name: packageJson.name, version: packageJson.version } : {}),
      ...(configMuchas || {}),
    };
  } catch (error) {
    /* tslint:disable-next-line */
    console.error(`Error loading config`, error);
    process.exit(1);
  }
};

export default configLoader;
