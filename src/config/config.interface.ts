import { ConnectionOptions } from "mongoose";

export default interface Config {
  [x: string]: any;
  name: string; // The default name used is the name in package.json
  database: {
    uri: string;
    options: ConnectionOptions;
  };
  broker: {};
}
