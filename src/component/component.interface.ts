import getCallerFile from "get-caller-file";

import Route from "../web/route.interface";
import { Message } from "../broker";
import Routine from "../routine/routine.interface";

/**
 * Muchas Basic Component Interface
 *
 * @interface ComponentInterface
 */
interface ComponentInterface {
  baseUrl?: string;
  routes?: Route[];
  messages?: Message[];
  routines?: Routine[];
}

/**
 * Muchas Basic Component
 *
 * @export
 * @class Component
 * @implements {IComponent}
 */
export default class Component implements ComponentInterface {
  public baseUrl?: string;
  public routes?: Route[];
  public messages?: Message[];
  public routines?: Routine[];

  constructor(options: ComponentInterface) {
    const defaultBaseUrlSplited = getCallerFile().split("/");

    const defaultBaseUrl =
      defaultBaseUrlSplited[defaultBaseUrlSplited.length > 0 ? defaultBaseUrlSplited.length - 2 : 0];

    this.baseUrl = options.baseUrl || defaultBaseUrl;

    const hasRoutes = options.routes && options.routes.length > 0;
    const hasMessages = options.messages && options.messages.length > 0;
    const hasRoutines = options.routines && options.routines.length > 0;

    if (!hasRoutes && !hasMessages && !hasRoutines) {
      throw new Error("Component must have at least one of the following features: Routes, Messages or Routines");
    }

    if (options.routes) {
      this.routes = options.routes;
    }

    if (options.messages) {
      this.messages = options.messages;
    }

    if (options.routines) {
      this.routines = options.routines;
    }
  }
}

export { ComponentInterface };
