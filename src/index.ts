import "source-map-support/register"; // Stack trace with source map
import apm from "elastic-apm-node/start";
import { Agent as Apm } from "./apm/apm.interface";
import mongoose from "mongoose";
import path from "path";
import { Server } from "http";
import { Express } from "express";
import configLoader from "./config";
import Logger, { LogOptions } from "./logger";
import Database from "./database";
import Broker, { Message, MessageAction } from "./broker";
import ServerInstance from "./web/Server";
import RoutineLoader from "./routine/RoutineLoader";
import Component, { ComponentInterface } from "./component/component.interface";
import Routine, { RoutineAction } from "./routine/routine.interface";
import Route, { Controller } from "./web/route.interface";

// Export framework types
export { ComponentInterface, Route, Controller, Routine, RoutineAction, Message, MessageAction, Apm };

// Export framework class
export { Component, mongoose };

// Types Helper
export * from "./types/Obj.type";

/**
 * Muchas Framework v3
 *
 * @interface Muchas
 */
export interface Muchas {
  apm?: Apm;
  log?: Logger | Console;
  /* eslint-disable-next-line */
  config?: any;
  database?: Database;
  broker?: Broker;
  server?: Server;
  serverInstance?: ServerInstance | undefined;
  app?: Express;
  /* eslint-disable-next-line */
  routines?: any;
  init(options: MuchasOptions): Promise<void>;
  shutdown(): Promise<void>;
}

export interface MuchasOptions {
  components: Component[];
  logger?: LogOptions;
  /* eslint-disable-next-line */
  models?: mongoose.Model<any>[];
}

const muchas: Muchas = {
  init: async (options: MuchasOptions) => {
    // Check if APM was passed
    muchas.apm = apm || undefined;

    try {
      muchas.log = console;
      muchas.config = await configLoader();

      // Start the logger so we can have a nice output
      muchas.log = new Logger({ ...muchas.config.logger, name: muchas.config.name });

      // All the loggers were binded
      muchas.log.debug(`Starting application "${muchas.config.name}"`);

      // Just say that we have apm
      if (muchas.apm) {
        muchas.log.debug("APM enabled");
      }

      if (muchas.config.database) {
        // Database
        muchas.database = await new Database(
          muchas.config.database.uri,
          muchas.config.database.options,
          muchas.log,
        ).connect();

        // Load models
        const { models } = options;
        muchas.database.loadModels(models);
      }

      // Broker
      if (muchas.config.broker) {
        muchas.broker = new Broker(
          {
            host: muchas.config.broker.host,
          },
          muchas.apm,
          muchas.log,
        );

        await muchas.broker.start();
      }

      // Web
      if (muchas.config.web) {
        muchas.serverInstance = new ServerInstance({
          port: muchas.config.web.port,
          headers: muchas.config.web.headers,
          secret: muchas.config.web.secret,
        });
        const { server, app } = await muchas.serverInstance.start();
        muchas.server = server;
        muchas.app = app;
      }

      // Routines
      if (muchas.config.routines) {
        muchas.routines = new RoutineLoader(
          {
            mongoConString: muchas.config.database.uri || null,
            app: muchas.app,
          },
          muchas.apm,
        );

        await muchas.routines.start();
      }

      // Load the components
      const { components } = options;
      components.forEach((comp: Component) => {
        // Load component routes
        if (comp.baseUrl === "/") {
          muchas.log.warn("Component using root base url. This may generate conflicted routes");
        }
        if (muchas.serverInstance && muchas.app && comp.routes) {
          comp.routes.forEach((route: Route): void => {
            const routePath = path.join("/", comp.baseUrl, route.path);
            muchas.serverInstance.addRoute(
              route.method,
              routePath,
              route.controller,
              route.middleware || [],
              route.secure,
              route.acl,
            );

            muchas.log.debug(
              `${route.secure ? "Secure " : ""}Route mapped: [${route.method.toUpperCase()}]${routePath}`,
            );
          });
        }

        // Load component messages
        if (muchas.broker && comp.messages) {
          comp.messages.forEach((message: Message): void => {
            muchas.broker.bindTask(message);
            muchas.log.debug(
              `Message mapped: ex: ${message.exchange} | rk: ${message.routeKey} | qe: ${message.queue}`,
            );
          });
        }
        // Load component routines
        if (muchas.routines && comp.routines) {
          comp.routines.forEach((routine: Routine): void => {
            muchas.routines.addJob(routine);
            muchas.log.debug(`Routine mapped: ${routine.id} @ ${routine.cron}`);
          });
        }
      });

      // Bind the gracefull shutdown
      if (process.env.NODE_ENV === "production") {
        process.on("SIGTERM", async () => await muchas.shutdown());
        process.on("SIGINT", async () => await muchas.shutdown());
      }

      // Liveness - Application is up and running
      if (muchas.serverInstance) {
        muchas.serverInstance.live();
      }

      // Useful console information
      let usefulLog = "Application is live";
      if (muchas.serverInstance) {
        usefulLog = `${usefulLog} \nLIVENESS: http://localhost:${muchas.config.web.port}/healthz`;
        usefulLog = `${usefulLog} \n    REST: http://localhost:${muchas.config.web.port}`;
      }
      if (muchas.routines) {
        usefulLog = `${usefulLog} \nROUTINES: http://localhost:${muchas.config.web.port}/routines`;
      }
      muchas.log.debug(usefulLog);
    } catch (error) {
      muchas.log.error(error.message, error.stack);
    }
  },
  shutdown: async () => {
    if (muchas.server && muchas.serverInstance) {
      await muchas.serverInstance.stop();
    }
    if (muchas.routines) {
      await muchas.routines.stop();
    }
    if (muchas.broker) {
      await muchas.broker.stop();
    }
    // Everthing is down, now we can change the healthz to down
    if (muchas.server && muchas.serverInstance) {
      muchas.serverInstance.down();
    }

    process.exit(0);
  },
};

export default muchas;
