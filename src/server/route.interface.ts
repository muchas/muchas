import { NextFunction, Request, Response } from "express";

export type IController = (req: Request, res: Response, next: NextFunction) => any;

type HttpMethods = "GET" | "POST" | "PUT" | "PATCH" | "DELETE" | "HEAD" | "CONNECT" | "OPTIONS" | "TRACE";

interface RouteInterface {
  path: string;
  method: HttpMethods;
  controller: IController;
}

export default RouteInterface;
