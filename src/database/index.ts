import mongoose, { Mongoose, ConnectionOptions, Schema, Document, Query, Model } from "mongoose";
import Logger from "@/logger";
import muchas from "..";

export { Schema, Document, Query };

export default class Database {
  private connectionUrl?: string;
  private mongoose: Mongoose;
  private options: ConnectionOptions;
  private log?: Logger | Console;
  public models?: { [x: string]: Model<any> } = {};

  /**
   * Creates an instance of Database.
   * @param {DatabaseOptions} options
   */
  constructor(uri: string, options: ConnectionOptions, log: Logger | Console) {
    this.connectionUrl = uri;
    this.options = options;
    this.log = log;
    this.mongoose = mongoose;
  }

  /**
   * Connects to the database
   *
   * @returns {Promise<mongoose.Mongoose>}
   */
  public async connect(): Promise<Database> {
    try {
      this.log.debug("Connecting to database");
      // Avoid deprecated warnings
      this.mongoose.set("useCreateIndex", true);

      this.mongoose.set("useFindAndModify", false);

      await this.mongoose.connect(this.connectionUrl, this.options);

      this.log.debug("Database connected");

      return this;
    } catch (error) {
      const { name, message } = error;

      if (name) {
        throw Error(`Database error: ${name} - ${message}`);
      }

      throw Error(error);
    }
  }

  public loadModels(models: Model<any>[] = []): void {
    models.forEach((model: Model<any>) => {
      this.models[model.modelName] = model;
      this.log.debug(`Model ${model.modelName} loaded`);
    });
  }
}
