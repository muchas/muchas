import apm from "elastic-apm-node";

interface ApmOptions {
  loglevel: LovLevel;
  apmHost: string;
  sample: number;
  token?: string;
}

type LovLevel = "trace" | "debug" | "info" | "warn" | "error" | "fatal";

export default (name: string, nodeEnv: string, apmOptions: ApmOptions | undefined): any | undefined => {
  if (!name || !apmOptions) {
    return undefined;
  }
  const logLevel: LovLevel = apmOptions.loglevel;
  const opt: any = {
    logLevel,
    serverUrl: apmOptions.apmHost,
    serviceName: nodeEnv === "production" ? name : `${name}-${nodeEnv || "development"}`,
    transactionSampleRate: apmOptions.sample,
  };

  if (apmOptions.token) {
    opt.secretToken = apmOptions.token;
  }

  return apm.start(opt);
};
